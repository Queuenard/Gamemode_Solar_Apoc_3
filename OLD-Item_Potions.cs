//Healing

datablock ItemData(potionHealingItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/potion.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Potion (Healing)";
	iconName = "./potion";
	doColorShift = true;
	colorShiftColor = "0.85 0.40 0.40 1.00";

	 // Dynamic properties defined by the scripts
	image = potionHealingImage;
	canDrop = true;
};

datablock ShapeBaseImageData(potionHealingImage)
{
   // Basic Item properties
   shapeFile = "./models/potion.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = potionHealingItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = true;
   colorShiftColor = potionHealingItem.colorShiftColor;

   // Initial start up state
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function potionHealingImage::onFire(%this,%obj,%slot)
{
	if(%obj.getDamageLevel() <= 0)
	{
		%obj.client.play2d(errorSound);
		%obj.client.centerPrint("\c0Whoops!<br>\c6You are at full health - There is no point to heal.",3);
		return;
	}
	
	%currSlot = %obj.currTool;

	%obj.addHealth(100);
	%obj.setWhiteOut(0.7);
	
	EOTW_UpdatePlayerStats(%obj.client);
	
	%obj.emote(HealImage,1);
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%obj.client))
	{
		messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
	else
		%obj.unMountImage(%slot);
}

//Regen

datablock ItemData(potionRegenItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/potion.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Potion (Regen)";
	iconName = "./potion";
	doColorShift = true;
	colorShiftColor = "0.85 0.00 0.85 1.00";

	 // Dynamic properties defined by the scripts
	image = potionRegenImage;
	canDrop = true;
};

datablock ShapeBaseImageData(potionRegenImage)
{
   // Basic Item properties
   shapeFile = "./models/potion.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = potionRegenItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = true;
   colorShiftColor = potionRegenItem.colorShiftColor;

   // Initial start up state
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function potionRegenImage::onFire(%this,%obj,%slot)
{
	if(%obj.currentPowerup !$= "")
	{
		%obj.client.play2d(errorSound);
		%obj.client.centerPrint("\c0Whoops!<br>\c6Powerups can not stack on eachother.",3);
		return;
	}
	
	%currSlot = %obj.currTool;

	%obj.setWhiteOut(0.4);
	PotionRegenHeal(%obj,30);
	
	%obj.currentPowerup = "Regen";
	
	%obj.emote(RegenHealImage,1);
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%obj.client))
	{
		messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
	else
		%obj.unMountImage(%slot);
}

function PotionRegenHeal(%obj,%cyclesLeft)
{
	%obj.addHealth(5);
	%obj.setWhiteOut(%obj.getWhiteOut() + 0.05);
	%obj.emote(RegenHealImage,1);
	
	EOTW_UpdatePlayerStats(%obj.client);
	if (%cyclesLeft > 0)
		schedule(1000,%obj,PotionRegenHeal,%obj,%cyclesLeft - 1);
	else
		%obj.currentPowerup = "";
}

//Speed

datablock ItemData(potionSpeedItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/potion.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Potion (Speed)";
	iconName = "./potion";
	doColorShift = true;
	colorShiftColor = "0.85 0.85 0.00 1.00";

	 // Dynamic properties defined by the scripts
	image = potionSpeedImage;
	canDrop = true;
};

datablock ShapeBaseImageData(potionSpeedImage)
{
   // Basic Item properties
   shapeFile = "./models/potion.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = potionSpeedItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = true;
   colorShiftColor = potionSpeedItem.colorShiftColor;

   // Initial start up state
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function potionSpeedImage::onFire(%this,%obj,%slot)
{
	if(%obj.currentPowerup !$= "")
	{
		%obj.client.play2d(errorSound);
		%obj.client.centerPrint("\c0Whoops!<br>\c6Powerups can not stack on eachother.",3);
		return;
	}
	
	%currSlot = %obj.currTool;

	%obj.setWhiteOut(0.5);
	%obj.emote(SpeedUpImage,1);
	SetPotionSpeed(%obj,true);
	
	%obj.currentPowerup = "Speed";
	
	//%obj.emote(SpeedEmoteImage,1);
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%obj.client))
	{
		messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
	else
		%obj.unMountImage(%slot);
}

function SetPotionSpeed(%obj,%val)
{
	%speed = 3;
	if (%val)
	{
		%obj.setMaxForwardSpeed(%obj.getMaxForwardSpeed() * %speed);
		%obj.setMaxSideSpeed(%obj.getMaxSideSpeed() * %speed);
		%obj.setMaxBackwardSpeed(%obj.getMaxBackwardSpeed() * %speed);
		schedule(10000,%obj,SetPotionSpeed,%obj,false);
	}
	else
	{
		%obj.setMaxForwardSpeed(%obj.getMaxForwardSpeed() / %speed);
		%obj.setMaxSideSpeed(%obj.getMaxSideSpeed() / %speed);
		%obj.setMaxBackwardSpeed(%obj.getMaxBackwardSpeed() / %speed);
		%obj.currentPowerup = "";
	}
}

//Power

datablock ItemData(potionPowerItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/potion.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Potion (Power)";
	iconName = "./potion";
	doColorShift = true;
	colorShiftColor = "0.25 0.00 0.00 1.00";

	 // Dynamic properties defined by the scripts
	image = potionPowerImage;
	canDrop = true;
};

datablock ShapeBaseImageData(potionPowerImage)
{
   // Basic Item properties
   shapeFile = "./models/potion.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = potionPowerItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = true;
   colorShiftColor = potionPowerItem.colorShiftColor;

   // Initial start up state
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function potionPowerImage::onFire(%this,%obj,%slot)
{
	if(%obj.currentPowerup !$= "")
	{
		%obj.client.play2d(errorSound);
		%obj.client.centerPrint("\c0Whoops!<br>\c6Powerups can not stack on eachother.",3);
		return;
	}
	
	%currSlot = %obj.currTool;

	%obj.setWhiteOut(0.5);
	
	SetPotionPower(%obj,true);
	
	%obj.currentPowerup = "Power";
	
	//%obj.emote(SpeedEmoteImage,1);
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%obj.client))
	{
		messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
	else
		%obj.unMountImage(%slot);
}

function SetPotionPower(%obj,%val)
{
	if (%val)
	{
		%obj.damageMult++;
		schedule(45000,%obj,SetPotionPower,%obj,false);
	}
	else
	{
		%obj.damageMult--;
		%obj.currentPowerup = "";
		%obj.client.chatMessage("\c6Your Power Powerup wore off.");
	}
}

//Pickup

datablock ItemData(potionPickupItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/potion.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Potion (Pickup)";
	iconName = "./potion";
	doColorShift = true;
	colorShiftColor = "0.00 0.85 0.00 1.00";

	 // Dynamic properties defined by the scripts
	image = potionPickupImage;
	canDrop = true;
};

datablock ShapeBaseImageData(potionPickupImage)
{
   // Basic Item properties
   shapeFile = "./models/potion.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = potionPickupItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = true;
   colorShiftColor = potionPickupItem.colorShiftColor;

   // Initial start up state
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function potionPickupImage::onFire(%this,%obj,%slot)
{
	if(%obj.usingPickupPwrUp)
	{
		%obj.client.play2d(errorSound);
		%obj.client.centerPrint("\c0Whoops!<br>\c6You are already under the influence of a Pickup Powerup.",3);
		return;
	}
	
	%currSlot = %obj.currTool;

	%obj.setWhiteOut(0.5);
	
	SetPotionPickup(%obj,true);
	
	//%obj.emote(SpeedEmoteImage,1);
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%obj.client))
	{
		messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
	else
		%obj.unMountImage(%slot);
}

function SetPotionPickup(%obj,%val)
{
	if (%val)
	{
		%obj.pickupSpeed -= 0.25;
		%obj.usingPickupPwrUp = true;
		schedule(45000,%obj,SetPotionPickup,%obj,false);
	}
	else
	{
		%obj.client.chatMessage("\c6Your Pickup Powerup wore off.");
		%obj.pickupSpeed += 0.25;
		%obj.usingPickupPwrUp = false;
	}
}

//SunInvul

datablock ItemData(potionSunInvulItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/potion.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Potion (SunInvul)";
	iconName = "./potion";
	doColorShift = true;
	colorShiftColor = "0.00 0.00 1.00 1.00";

	 // Dynamic properties defined by the scripts
	image = potionSunInvulImage;
	canDrop = true;
};

datablock ShapeBaseImageData(potionSunInvulImage)
{
   // Basic Item properties
   shapeFile = "./models/potion.dts";
   emap = true;

   // Specify mount point & offset for 3rd person, and eye offset
   // for first person rendering.
   mountPoint = 0;
   offset = "0.09 -0.07 -0.2";
   eyeOffset = 0; //"0.7 1.2 -0.5";
   rotation = eulerToMatrix( "0 0 0" );

   className = "WeaponImage";
   item = potionSunInvulItem;

   //raise your arm up or not
   armReady = true;

   doColorShift = true;
   colorShiftColor = potionSunInvulItem.colorShiftColor;

   // Initial start up state
    stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function potionSunInvulImage::onFire(%this,%obj,%slot)
{
	if(%obj.immuneToSun == true)
	{
		%obj.client.play2d(errorSound);
		%obj.client.centerPrint("\c0Whoops!<br>\c6You are already currently immune to the sun.",3);
		return;
	}
	
	%currSlot = %obj.currTool;

	%obj.setWhiteOut(0.5);
	
	SetPotionSunInvul(%obj,true);
	
	//%obj.emote(SpeedEmoteImage,1);
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%obj.client))
	{
		messageClient(%obj.client,'MsgItemSunInvul','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
	else
		%obj.unMountImage(%slot);
}

function SetPotionSunInvul(%obj,%val)
{
	if (%val)
	{
		%obj.immuneToSun = true;
		%obj.immuneToBurn = true;
		%obj.immuneToFire = true;
		schedule(60000,%obj,SetPotionSunInvul,%obj,false);
	}
	else
	{
		%obj.client.chatMessage("\c6Your SunInvul Powerup wore off.");
		%obj.immuneToSun = false;
		%obj.immuneToBurn = false;
		%obj.immuneToFire = false;
	}
}
