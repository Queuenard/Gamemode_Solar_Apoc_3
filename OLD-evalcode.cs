function applyBotSkin(%obj)
{
	//echo(%obj.getDataBlock().getName() SPC %obj.dataBlock SPC "UnfleshedHoleBot");
	if (%obj.getDataBlock().getName() $= "UnfleshedHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";

		%llegColor =  "0 0.141 0.333 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "asciiTerror";
		%rarmColor =  "0.626 0.71 0.453 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0.141 0.333 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		%decalName =  "HCZombie";
		%larmColor =  "0.593 0 0 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0.626 0.71 0.453 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "0.626 0.71 0.453 1";
		%rleg =  "0";
		%rlegColor =  "0 0.141 0.333 1";
		%accent =  "1";
		%headColor =  "0.626 0.71 0.453 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.626 0.71 0.453 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
	}
	else if (%obj.getDataBlock().getName() $= "SwarmerHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";

		%llegColor =  "0.626 1.000 0.453 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		//%faceName =  "asciiTerror";
		%rarmColor =  "0.626 1.000 0.453 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0.141 0.333 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		//%decalName =  "HCZombie";
		%larmColor =  "0.626 1.000 0.453 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0.626 1.000 0.453 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "0.626 1.000 0.453 1";
		%rleg =  "1";
		%rlegColor =  "0.626 1.000 0.453 1";
		%accent =  "1";
		%headColor =  "0.626 1.000 0.453 1";
		%rhand =  "0";
		%lleg =  "1";
		%lhandColor =  "0.626 1.000 0.453 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
		
		%obj.setPlayerScale("0.8 0.8 0.8");
	}
	else if (%obj.getDataBlock().getName() $= "HuskHoleBot") //0.914 0.898 0.629 1
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%llegColor =  "0.914 0.898 0.629 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "Orc";
		%rarmColor =  "0.914 0.898 0.629 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0 0 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		%decalName =  "HCZombie";
		%larmColor =  "0.914 0.898 0.629 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0.914 0.898 0.629 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "1 0 0 1";
		%rleg =  "0";
		%rlegColor =  "0.914 0.898 0.629 1 1";
		%accent =  "1";
		%headColor =  "0.914 0.898 0.629 1 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "1 0 0 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("1.1 1.1 1.1");
	}
	else if (%obj.getDatablock().getName() $= "RevenantHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "SkeletonEvil";
		%rarmColor =  "1 1 1 1";
		%hipColor =  "1 1 1 1";
		%chest =  "0";
		%rarm =  "1";
		%decalName =  "AAA-None";
		%larmColor =  "1 1 1 1";
		%larm =  "1";
		%chestColor =  "1 1 1 1";
		%accentColor =  "1 1 1 1";
		%rhandColor =  "1 1 1 1";
		%rleg =  "0";
		%accent =  "1";
		%headColor =  "1 1 1 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "1 1 1 1";
	
		%hat =  "0";
		%hatColor =  "1 1 1 1";
		%pack =  "0";
		%packColor =  "1 1 1 1";
		%secondPack =  "0";
		%secondPackColor =  "1 1 1 1";
		%llegColor =  "1 1 1 1";
		%rlegColor =  "1 1 1 1";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
		
		%obj.schedule(1000,"hideNode","chest");
		%obj.mountimage(ribcageImage,2); //why is this broken????
		
		%obj.setPlayerScale("0.8 0.8 0.8");
	}
	else if (%obj.getDatablock().getName() $= "ElementalHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "blank";
		%rarmColor =  "0.5 0 0 0.75";
		%hipColor =  "0.5 0 0 0.75";
		%chest =  "0";
		%rarm =  "1";
		%decalName =  "AAA-None";
		%larmColor =  "0.5 0 0 0.75";
		%larm =  "1";
		%chestColor =  "0.5 0 0 0.75";
		%accentColor =  "0.5 0 0 0.75";
		%rhandColor =  "0.5 0 0 0.75";
		%rleg =  "0";
		%accent =  "1";
		%headColor =  "0.5 0 0 0.75";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.5 0 0 0.75";
	
		%hat =  "0";
		%hatColor =  "0.5 0 0 0.75";
		%pack =  "0";
		%packColor =  "0.5 0 0 0.75";
		%secondPack =  "0";
		%secondPackColor =  "0.5 0 0 0.75";
		%llegColor =  "0.5 0 0 0.75";
		%rlegColor =  "0.5 0 0 0.75";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("2.0 2.0 2.0");
		%obj.setNodeColor("ALL","0 0 0 0.5");
		%obj.hideNode("lshoe");
		%obj.hideNode("Rshoe");
	}
	else if (%obj.getDatablock().getName() $= "ChaosElementalHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "blank";
		%rarmColor =  "0.5 0 0 0.75";
		%hipColor =  "0.5 0 0 0.75";
		%chest =  "0";
		%rarm =  "1";
		%decalName =  "AAA-None";
		%larmColor =  "0.5 0 0 0.75";
		%larm =  "1";
		%chestColor =  "0.5 0 0 0.75";
		%accentColor =  "0.5 0 0 0.75";
		%rhandColor =  "0.5 0 0 0.75";
		%rleg =  "0";
		%accent =  "1";
		%headColor =  "0.5 0 0 0.75";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.5 0 0 0.75";
	
		%hat =  "0";
		%hatColor =  "0.5 0 0 0.75";
		%pack =  "0";
		%packColor =  "0.5 0 0 0.75";
		%secondPack =  "0";
		%secondPackColor =  "0.5 0 0 0.75";
		%llegColor =  "0.5 0 0 0.75";
		%rlegColor =  "0.5 0 0 0.75";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("2.0 2.0 2.0");
		%obj.setNodeColor("ALL","0 0 0 0.5");
		%obj.hideNode("lshoe");
		%obj.hideNode("Rshoe");
	}
	else if (%obj.getDataBlock().getName() $= "BanditHoleBot")
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";

		%llegColor =  "0 0 1 1";
		%secondPackColor =  "0 0 0 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "asciiTerror";
		%rarmColor =  "0 0 1 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0 1 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0 0 0 1";
		%pack =  "0";
		//%decalName =  "HCZombie";
		%larmColor =  "0.9 0 0 1";
		%secondPack =  "0";
		%larm =  "0";
		%chestColor =  "0.9 0.9 0.9 1";
		%accentColor =  "0 0 0 1";
		%rhandColor =  "1.000 0.878 0.611 1";
		%rleg =  "0";
		%rlegColor =  "0 0.141 0.333 1";
		%accent =  "1";
		%headColor =  "1.000 0.878 0.611 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "1.000 0.878 0.611 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
	}
	else if (%obj.getDataBlock().getName() $= "BehemothHoleBot") //0.914 0.898 0.629 1
	{
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%llegColor =  "0 0 0 1";
		%secondPackColor =  "0 0 0 1";
		%lhand =  "1";
		%hip =  "1";
		%faceName =  "Orc";
		%rarmColor =  "0 0 0 1";
		%hatColor =  "0 0 0 1";
		%hipColor =  "0 0 0 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0 0 0 1";
		%pack =  "0";
		%decalName =  "HCZombie";
		%larmColor =  "0 0 0 1";
		%secondPack =  "0";
		%larm =  "1";
		%chestColor =  "0 0 0 1";
		%accentColor =  "0 0 0";
		%rhandColor =  "1 0 0 1";
		%rleg =  "0";
		%rlegColor =  "0 0 0 1";
		%accent =  "1";
		%headColor =  "0 0 0 1";
		%rhand =  "1";
		%lleg =  "0";
		%lhandColor =  "0 0 0 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = true;
		%obj.immuneToFire = true;
		%obj.immuneToNight = true;
		%obj.tripleFire = true;
		
		%obj.setPlayerScale("4 4 4");
	}
	else //Default Zombie
	{
	
		%obj.playthread(1,"ArmReadyBoth");
		%obj.hDefaultThread = "ArmReadyBoth";
		
		%llegColor =  "0 0.141 0.333 1";
		%secondPackColor =  "0 0.435 0.831 1";
		%lhand =  "0";
		%hip =  "0";
		%faceName =  "asciiTerror";
		%rarmColor =  "0.593 0 0 1";
		%hatColor =  "1 1 1 1";
		%hipColor =  "0 0.141 0.333 1";
		%chest =  "0";
		%rarm =  "0";
		%packColor =  "0.2 0 0.8 1";
		%pack =  "0";
		%decalName =  "AAA-None";
		%larmColor =  "0.593 0 0 1";
		%secondPack =  "0";
		%larm =  "0";
		%chestColor =  "0.75 0.75 0.75 1";
		%accentColor =  "0.990 0.960 0 0.700";
		%rhandColor =  "0.626 0.71 0.453 1";
		%rleg =  "0";
		%rlegColor =  "0 0.141 0.333 1";
		%accent =  "1";
		%headColor =  "0.626 0.71 0.453 1";
		%rhand =  "0";
		%lleg =  "0";
		%lhandColor =  "0.626 0.71 0.453 1";
		%hat =  "0";
		
		//Solar Apoc
		%obj.immuneToSun = false;
		%obj.immuneToFire = false;
		%obj.immuneToNight = true;
		%obj.tripleFire = false;
	}
	
	if (getRandom(0,100) == 1) //rare memes!!! //
		%faceName = "memeYaranika";
	
	servercmdupdatebodyparts(%obj, %hat, %accent, %pack, %secondPack, %chest, %hip, %LLeg, %RLeg, %LArm, %RArm, %LHand, %RHand);
	servercmdupdatebodycolors(%obj, %headColor, %hatColor, %accentColor, %packColor, %secondPackColor, %chestColor, %hipColor, %LLegColor, %RLegColor, %LArmColor, %RArmColor, %LHandColor, %RHandColor, %decalName, %faceName);
	
	GameConnection::ApplyBodyParts(%obj);
	GameConnection::ApplyBodyColors(%obj);
	
	if (%obj.getDatablock().getName() $= "ChaosElementalHoleBot")
	{
		%obj.setPlayerScale("2.0 2.0 2.0");
		%obj.setNodeColor("ALL","0 0 0 0.5");
	}
	
}