datablock fxDTSBrickData(brickEOTWTradePostData)
{
	brickFile = "./bricks/TradePost.blb";
	category = "Solar Apoc";
	subCategory = "Miscellaneous";
	uiName = "Trading Post";
	iconName = "Add-Ons/Gamemode_Solar_Apoc3/uiIcons/TradePost";
};

function brickEOTWTradePostData::onPlant(%this,%b,%c,%d)
{
	%b.schedule(10,UpdateTradeText);
	return parent::onPlant(%this,%b,%c,%d);
}

function brickEOTWTradePostData::onLoadPlant(%this,%b,%c,%d)
{
	%b.schedule(10,UpdateTradeText);
	return parent::onLoadPlant(%this,%b,%c,%d);
}

function fxDtsBrick::UpdateTradeText(%this)
{
	if (%this.tradeData[0] !$= "")
	{
		if (%this.tradesLeft != -1)
			%this.SetBrickText("(Trading Post) Selling " @ %this.tradeData[1] SPC %this.tradeData[0] @ " for " @ %this.tradeData[3] SPC %this.tradeData[2] @ ". (" @ %this.tradesLeft @ " trades left)");
		else
			%this.SetBrickText("(Trading Post) Selling " @ %this.tradeData[1] SPC %this.tradeData[0] @ " for " @ %this.tradeData[3] SPC %this.tradeData[2] @ ".");
	}
	else
		%this.SetBrickText("(Trading Post) No Trade Open","0.75 0.75 0.75");
}

function ServerCmdSetTrade(%client, %sellAmt, %sellMat, %buyAmt, %buyMat, %tradeLimit)
{
	if (!isObject(%player = %client.player))
		return;
		
	%sellMat = getProperMatName(%sellMat);
	if (%sellMat $= "???") %sellMat = "Sturdium";
	
	%buyMat = getProperMatName(%buyMat);
	if (%buyMat $= "???") %buyMat = "Sturdium";
	
	%sellAmt = mFloor(%sellAmt);
	%buyAmt = mFloor(%buyAmt);
	%tradeLimit = mFloor(%tradeLimit);
	
	if (%sellMat $= "Unknown" || %sellAmt < 1 || %sellMat $= "Unknown" || %buyAmt < 1)
	{
		%client.chatMessage("Usage: /SetTrade <SellAmt> <SellMat> <BuyAmt> <BuyMat> <TradeLimit (Optinal)>");
		return;
	}
	else
	{
		%eye = %player.getEyePoint();
		%dir = %player.getEyeVector();
		%for = %player.getForwardVector();
		%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
		%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
		%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 5)), %mask, %player);
		%hit = firstWord(%ray);
		
		if(isObject(%hit) && %hit.getClassName() $= "fxDtsBrick" && %hit.getDatablock().getName() $= "brickEOTWTradePostData")
		{
			%hit.tradeData[0] = %sellMat;
			%hit.tradeData[1] = %sellAmt;
			%hit.tradeData[2] = %buyMat;
			%hit.tradeData[3] = %buyAmt;
			
			if (%TradeLimit > 0)
				%hit.tradesLeft = %TradeLimit;
			else
				%hit.tradesLeft = -1;
				
			%hit.tradeCount = 0;
				
			if (%hit.tradesLeft != -1)
				%client.chatMessage("\c6You are now selling " @ %hit.tradeData[1] SPC %hit.tradeData[0] @ " for " @ %hit.tradeData[3] SPC %hit.tradeData[2] @ ". (" @ %hit.tradesLeft @ " trades left)");
			else
				%client.chatMessage("\c6You are now selling " @ %hit.tradeData[1] SPC %hit.tradeData[0] @ " for " @ %hit.tradeData[3] SPC %hit.tradeData[2] @ ".");
				
			%hit.UpdateTradeText();
		}
		else
		{
			%client.chatMessage("\c6Look at a trading post you placed to setup your trade.");
			return;
		}
	}
}

function ServerCmdRemoveTrade(%client)
{
	if (!isObject(%player = %client.player))
		return;
		
		%eye = %player.getEyePoint();
		%dir = %player.getEyeVector();
		%for = %player.getForwardVector();
		%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
		%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
		%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 5)), %mask, %player);
		%hit = firstWord(%ray);
		
		if(isObject(%hit) && %hit.getClassName() $= "fxDtsBrick" && %hit.getDatablock().getName() $= "brickEOTWTradePostData")
		{
			if (%client.bl_id == %hit.getGroup().bl_id)
			{
				for (%i = 0; %i < 4; %i++)
					%hit.tradeData[%i] = "";
					
				%hit.tradesLeft = "";
				%hit.tradeCount = "";
				
				%client.chatMessage("\c6Your trading post has been reset.");
				
				%hit.UpdateTradeText();
			}
			else
			{
				%client.chatMessage("\c6This is not your trading post.");
			}
		}
		else
		{
			%client.chatMessage("Look at a trading post you placed to reset it.");
			return;
		}
}

function EOTW_canDoTrade(%bl_id, %brick)
{
	if (!isObject(%brick)) return "noBrick";
	
	if (%brick.tradesLeft == 0) return "noTrades";
	
	if ($EOTW::Material[%bl_id, %brick.tradeData[2]] < %brick.tradeData[3]) return "needMaterial";
	
	if ($EOTW::Material[%brick.getGroup().bl_id, %brick.tradeData[0]] < %brick.tradeData[1]) return "supplierMaterial";
	
	return true;
	
}

function ServerCmdTradePostDoTrade(%client)
{
	if (!isObject(%player = %client.player) || !isObject(%brick = %player.tradeBrick) || !EOTW_canDoTrade(%client.bl_id, %brick)) return;
	
	if (%brick.tradesLeft > 0)
		%brick.tradesLeft--;
		
	%hit.tradeCount++;
		
	$EOTW::Material[%client.bl_id, %brick.tradeData[2]] -= %brick.tradeData[3];
	$EOTW::Material[%client.bl_id, %brick.tradeData[0]] += %brick.tradeData[1];
	%client.play2d(BrickChangeSound);
	
	if (%brick.tradesLeft == -1)
		%client.chatMessage("\c6Trade Sucessful!");
	else
		%client.chatMessage("\c6Trade Sucessful! The trading post has " @ %brick.tradesLeft @ " trade(s) left.");
	
	$EOTW::Material[%brick.getGroup().bl_id, %brick.tradeData[2]] += %brick.tradeData[3];
	$EOTW::Material[%brick.getGroup().bl_id, %brick.tradeData[0]] -= %brick.tradeData[1];
	
	if (isObject(%brick.getGroup().client))
	{
		if (%brick.tradesLeft != -1)
			%brick.getGroup().client.chatMessage("\c6" @ %client.getSimpleName() @ " has bought " @ %brick.tradeData[1] SPC %brick.tradeData[0] @ " from you for " @ %brick.tradeData[3] SPC %brick.tradeData[2] @ ". Trades Left: " @ %brick.tradesLeft);
		else
			%brick.getGroup().client.chatMessage("\c6" @ %client.getSimpleName() @ " has bought " @ %brick.tradeData[1] SPC %brick.tradeData[0] @ " from you for " @ %brick.tradeData[3] SPC %brick.tradeData[2] @ ".");
	}
	
	%player.tradeBrick = "";
	%brick.UpdateTradeText();
}

function ServerCmdTradePostCancelTrade(%client)
{
	if (!isObject(%player = %client.player) || !isObject(%brick = %player.tradeBrick)) return;
	%player.tradeBrick = "";
	%client.chatMessage("\c6Trade Cancelled.");
}

package EOTW_TradePost
{
	function armor::onTrigger(%data,%this,%slot,%state)
	{
		if(%slot == 0 && %state == 1 && %this.getMountedImage(0) == 0 && isObject(%client = %this.client))
		{
			%eye = %this.getEyePoint();
			%dir = %this.getEyeVector();
			%for = %this.getForwardVector();
			%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
			%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
			%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 5)), %mask, %this);
			%hit = firstWord(%ray);
			
			if(isObject(%hit) && %hit.getClassName() $= "fxDtsBrick" && %hit.getDatablock().getName() $= "brickEOTWTradePostData")
			{
				if (%hit.tradeData[0] $= "")
				{
					if (%hit.getGroup().bl_id == %client.bl_id)
						%client.chatMessage("\c6You do not currently have a trade open. Type /settrade to setup your trade post.");
					else
						%client.chatMessage("\c6This trade post is currently not open.");
				}
				else
				{
					if (%hit.getGroup().bl_id == %client.bl_id)
						%client.chatMessage("\c6Your trade post has been used " @ %hit.tradeCount @ " time(s). Type /removetrade to reset a targetted trade post.");
					else
					{
						
						%tradeReturn = EOTW_canDoTrade(%client.bl_id, %hit);
						
						if (%tradeReturn $= true)
						{
							%this.tradeBrick = %hit;
							commandToClient(%client,'messageBoxYesNo',"Trading Post", "Purchase " @ %hit.tradeData[1] SPC %hit.tradeData[0] @ " with " @ %hit.tradeData[3] SPC %hit.tradeData[2] @ "?", 'TradePostDoTrade','TradePostCancelTrade');
						}
						else if (%tradeReturn $= "noBrick") %client.chatMessage("\c6You do not have a trading post selected.");
						else if (%tradeReturn $= "noTrades") %client.chatMessage("\c6The trades of this post have been used up.");
						else if (%tradeReturn $= "needMaterial") %client.chatMessage("\c6You need " @ (%hit.tradeData[3] - $EOTW::Material[%client.bl_id, %brick.tradeData[2]]) @ " more " @ %hit.tradeData[2] @ "to do this trade.");
						else if (%tradeReturn $= "supplierMaterial") %client.chatMessage("\c6The trading post's owner does not have the funds for this trade.");
						else %client.chatMessage("\c6This trade is not avaliable.");
					}
				}
			}
		}
		
		return Parent::onTrigger(%data,%this,%slot,%state);
	}
};
activatePackage("EOTW_TradePost");
