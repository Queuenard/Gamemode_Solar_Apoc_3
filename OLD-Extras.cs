//We are using a modified version of the original click to pickup due to the addition of crafting.
$EOTW::RemoveVehicleOnDeath = true;

if(!$RTB::Hooks::ServerControl)
{
	if($Pref::Server::CTPU::NotFirstExecution $= "")
	{
		$Pref::Server::CTPU::NotFirstExecution = 1;
		$Pref::Server::CTPU::Toggle = 1;
		$Pref::Server::CTPU::Range = 3;
		$Pref::Server::CTPU::Block = 0;
		$Pref::Server::CTPU::Copy = 1;
	}
}
else
{
	$Pref::Server::CTPU::NotFirstExecution = 1;
	RTB_registerPref("Click to Pickup Items","Click to Pickup","Pref::Server::CTPU::Toggle",bool,"CTPU",1,0,0);
	RTB_registerPref("Item Pickup Range","Click to Pickup","Pref::Server::CTPU::Range","string 3","CTPU","3",0,0);
	RTB_registerPref("Players Can Block","Click to Pickup","Pref::Server::CTPU::Block",bool,"CTPU",0,0,0);
	RTB_registerPref("Can Pickup Copies","Click to Pickup","Pref::Server::CTPU::Copy",bool,"CTPU",1,0,0);
}

deactivatePackage(OneSpawnVehicle);
package OneSpawnVehicle
{
	function Vehicle::damage(%vehicle,%source,%pos,%damage,%type)
	{
		if(%vehicle.getDamageLevel() + %damage >= %vehicle.getDatablock().maxDamage)
		{
			if(isObject(%vehicle.spawnBrick) && !%vehicle.triggeredOnDeath && $EOTW::RemoveVehicleOnDeath)
			{
				%vehicle.spawnBrick.schedule(100,setVehicle,"NONE");
			}
		}
		Parent::damage(%vehicle,%source,%pos,%damage,%type);
	}
	function Armor::damage(%this, %obj, %sourceObj, %position, %damage, %damageType)
	{
		Parent::damage(%this, %obj, %sourceObj, %position, %damage, %damageType);
		if(isObject(%obj) && %obj.getClassName() $= "AIPlayer" && %obj.getDataBlock().getName() $= "HorseArmor" && %obj.getState() $= "DEAD")
		{
			if(isObject(%obj.spawnBrick) && $EOTW::RemoveVehicleOnDeath)
			{
				%obj.spawnBrick.schedule(1000,setVehicle,"NONE");
			}
		}
		
	}
};
activatePackage(OneSpawnVehicle);

package ClickToPickup
{
	function armor::onCollision(%data,%this,%col,%vec,%vel)
	{
		if(%col.getClassName() $= "Item" && $Pref::Server::CTPU::Toggle && !%col.craftedItem && strPos(%col.getDataBlock().uiName,"Ammo") == -1)
		{
			if(!%this.client.messagedAboutCTPU)
			{
				messageClient(%this.client,'',"Click to pick up items.");
				%this.client.messagedAboutCTPU = 1;
			}
			return 0;
		}
		else if (strPos(%col.getDataBlock().uiName,"Ammo") > -1 && isObject(%col.spawnBrick))
		{
			if(!%this.client.messagedAboutCTPU)
			{
				messageClient(%this.client,'',"Click to pick up items.");
				%this.client.messagedAboutCTPU = 1;
			}
			return 0;
		}
		return Parent::onCollision(%data,%this,%col,%vec,%vel);
	}
	function armor::onTrigger(%data,%this,%slot,%state)
	{
		//echo(%data SPC %this SPC %slot SPC %state);
		%ret = Parent::onTrigger(%data,%this,%slot,%state);
		if(%slot == 0 && %state == 1 && $Pref::Server::CTPU::Toggle && %this.getMountedImage(0) == 0)
		{
			if($Pref::Server::CTPU::Range > 20)
				$Pref::Server::CTPU::Range = 20;
			else if($Pref::Server::CTPU::Range < 0)
				$Pref::Server::CTPU::Range = 0;
			else if($Pref::Server::CTPU::Range $= "default")
				$Pref::Server::CTPU::Range = 2.5;
			%start = %this.getEyePoint();
			%end = vectorScale(%this.getEyeVector(), $Pref::Server::CTPU::Range*getWord(%this.getScale(),2));
			%mask = $TypeMasks::FxBrickObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::ItemObjectType;
			if($Pref::Server::CTPU::Block)
				%mask = %mask | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
			%rayCast = containerRayCast(%start, vectorAdd(%start, %end), %mask, %this);
			%target = firstWord(%rayCast);
			
			if(isObject(%target) && %target.getClassName() $= "Item") // && miniGameCanUse(%this,%target) Broken for some reason, but we don't need it.
			{
				if($Pref::Server::CTPU::Copy)
					%copy = 0;
				else
				{
					%copy = 0;
					for(%x=0;%x<%data.maxTools;%x++)
						if(%this.item[%x].getID() == %col.getDataBlock().getID())
						{
							%copy = 1;
							break;
						}
				}
				//if(%target.canPickup && !%copy)
					//%this.pickup(%target);
				//echo(%target.getDataBlock().getName() TAB "$EOTWCrafting[" @ %target.getDataBlock().getName() @ "]" TAB $EOTWCrafting[%target.getDataBlock().getName()]);
				if(%target.canPickup && !%copy)
				{
					%n = %target.getDataBlock().getName();
					//echo(%target.getDataBlock().getName());
					//echo($EOTWCrafting[%target.getDataBlock().getName()]);
					%this.recipeCost[0] = "";
					%this.recipeCost[1] = "";
					%this.recipeCost[2] = "";
					for (%i = 0; %i < getFieldCount($EOTWCrafting[%n]); %i += 2)
					{
						%this.recipeCost[%i / 2] = getField($EOTWCrafting[%n],%i) TAB getField($EOTWCrafting[%n],%i + 1);
						
						if ($EOTW::Day < 53 && getField($EOTWCrafting[%n],%i) $= "Sturdium")
							%nameText = "???";
						else if (getField($EOTWCrafting[%n],%i) $= "Sturdium")
							%nameText = $SturdiumName;
						else
							%nameText = getField($EOTWCrafting[%n],%i);
							
						%text = %text @ "<br>" @ getField($EOTWCrafting[%n],%i + 1) SPC %nameText;
					}
					if(isObject(%target.spawnBrick) && $EOTWCrafting[%n] !$= "")
					{
						%this.client.itemCrafted = %target.getDatablock();
						%this.client.craftingBrick = %target.spawnBrick;
						%croppedUi = getSubStr(%target.getDataBlock().uiName,striPos(%target.getDataBlock().uiName,"]") + 2,100);
						%disc = $EOTW::ItemDisc[%croppedUi];
						
						if (%disc !$= "")
							%disc = ":" SPC %disc SPC "<br>";
						else
							%disc = " ";
						//messageBoxYesNo("Crafting", ("" SPC %target.getDataBlock().uiName @ " crafting cost:<br>" @ %text @ "<br><br>Craft this item?"), %this @ ".craftItem();",""); //" @ %this @ "," 
						commandToClient(%this.client,'messageBoxYesNo',"Crafting", %target.getDataBlock().uiName @ %disc @ "Crafting cost:<br>" @ %text @ "<br><br>Craft this item?", 'CraftItem','');
						
						//%obj = new SimObject()
						//{
						//	cmd  =  'MessageBoxYesNo';
						//	top  = "Crafting";
						//	msg  = %target.getDataBlock().uiName @ " crafting cost:<br>" @ %text @ "<br><br>Craft this item?";
						//	obj  = %this;
						//	//nums = %nums;
						//};
						//commandToClient(%this,%obj.cmd,"Prompt",%obj.msg,'MessageBoxAccept');
						//%this.client.promptGroup.add(%obj);
						//%this.client.triggerPrompt();
	
					}
					else
						%this.pickup(%target);
				}
					
				
			}
		}
		return %ret;
	}
	
	function GameConnection::triggerPrompt(%this)
	{
		%group = %this.promptGroup;
	
		if(%this.pendingPrompt || !isObject(%group) || !%group.getCount())
			return;
	
		%obj = %group.getObject(0);
		commandToClient(%this,%obj.cmd,"Prompt",%obj.msg,'MessageBoxAccept');
	
		%this.pendingPrompt = 1;
	}

	function ServerCmdCraftItem(%client)
	{
		%blid = %client.bl_id;
		for (%i = 0; getField(%client.player.recipeCost[%i], 0) !$= ""; %i++)
		{
			if ($EOTW::Material[%blid, getField(%client.player.recipeCost[%i], 0)] $= "")
				$EOTW::Material[%blid, getField(%client.player.recipeCost[%i], 0)] = 0; //So we atleast have a number.
				
			//echo(getField(%client.player.recipeCost[%i], 0) SPC $EOTW::Material[%blid, getField(%client.player.recipeCost[%i], 0)] SPC getField(%client.player.recipeCost[%i], 1));
			if (!($EOTW::Material[%blid, getField(%client.player.recipeCost[%i], 0)] >= getField(%client.player.recipeCost[%i], 1)))
			{
				%client.centerPrint("\c6You do not have enough materials to craft this item.", 3);
				%client.play2d(errorSound);
				return;
			}
		}
		
		for (%i = 0; getField(%client.player.recipeCost[%i], 0) !$= ""; %i++)
			$EOTW::Material[%blid, getField(%client.player.recipeCost[%i], 0)] -= getField(%client.player.recipeCost[%i], 1);

		%item = new Item()
		{
			datablock = %client.itemCrafted;
			static    = "0";
			position  = vectorAdd(%client.player.getPosition(),"0 0 1");
			craftedItem = true;
		};
		%item.schedulePop();
		%client.centerPrint("<color:00FF00>You successfully crafted the item!<br><color:FFFFFF>(You may have to move a bit to pick the item up)", 3);
		//%client.craftingBrick.setItem("NONE");
		%client.player.playAudio(2,wrenchHitSound);
		
		if ($EOTWCraftPoints[%client.itemCrafted.getName()] !$= "")
			%client.incScore($EOTWCraftPoints[%client.itemCrafted.getName()]);
		
		if (%client.itemCrafted.getName() $= "handDrillItem" || %client.itemCrafted.getName() $= "handDrill2Item")
			%client.AwardAchievement("Faster is Better");
		else if (%client.itemCrafted.getName() $= "shieldHIOOItem" || %client.itemCrafted.getName() $= "shieldLIOOItem")
			%client.AwardAchievement("Shields up!");
			
		%client.itemCrafted = "";
		%client.player.recipeCost[0] = "";
		%client.player.recipeCost[1] = "";
		%client.player.recipeCost[2] = "";
	}
};

activatePackage(ClickToPickup);

//-------------------------------------------------------------
// Script_Oxygen
// Copyright (c) SolarFlare Productions, Inc. //There was a copyright???
//-------------------------------------------------------------

//-----------------------------------------------------------------------------
// Change this value to change the maximum oxygen for the server.
$maxOxygen = 100;
$oxygenState = 0; // 1=On,0=Off

AddDamageType("Drowning", '', '', 1, 1);

package oxygen
{
    function Armor::onEnterLiquid(%this, %obj, %coverage, %type)
    {
        if($oxygenState == 1)
        {
            %obj.oxygen = $maxOxygen;
            %obj.oxygenTick = schedule(100, 0, oxygenTick, %obj);
        }
        else
        {
            %obj.oxygen = $maxOxygen;
        }
    }
    function oxygenTick(%obj)
    {
        if($oxygenState == 1)
        {
            if(!isObject(%obj))
                return;
            centerPrint(%obj.client, "<just:right>\c3Oxygen (\c6" @ %obj.oxygen--@ "\c3/\c6" @ $maxOxygen @ "\c3)", 1, 2);
            if(%obj.getWaterCoverage() $= 1)
                %obj.oxygen = %obj.oxygen;
            else
                %obj.oxygen = $maxOxygen;
                centerPrint(%obj.client, "<just:right>\c3Oxygen (\c6" @ %obj.oxygen @ "\c3/\c6" @ $maxOxygen @ "\c3)", 1, 2);
            if(%obj.oxygen <= 0)
            {
                %obj.damage(0, 0, 10000, $DamageType::Drowning);
                centerPrint(%obj.client, "<just:right>\c3Oxygen (\c0" @ %obj.oxygen @ "\c3/\c6" @ $maxOxygen @ "\c3)", 1, 2);
                return;
            }
            if(%obj.oxygen > $maxOxygen)
            {
                %obj.setOxygenLevel($maxOxygen);
            }
            %obj.oxygenTick = schedule(100, 0, oxygenTick, %obj);
        }
    }
    function Armor::onLeaveLiquid(%this, %obj, %type)
    {
        if($oxygenState == 1)
        {
            cancel(%obj.oxygenTick);
            %obj.oxygen = $maxOxygen;
            centerPrint(%obj.client, "<just:right>\c3Oxygen (\c6" @ %obj.oxygen @ "\c3/\c6" @ $maxOxygen @ "\c3)", 1, 2);
        }
    }
};
activatePackage(oxygen);

//The Slash Command to toggle, unneeded if you have RTB.
function serverCmdToggleOxygen(%client)
{
    if(%client.isSuperAdmin)
    {
        if($oxygenState == 0)
        {
            $oxygenState = 1;
            messageAll('', "\c0Oxygen has been \c3enabled\c0.");
        }
        else
        {
            $oxygenState = 0;
            messageAll('', "\c0Oxygen has been \c3disabled\c0.");	
        }
    }
}
//EVENT
function Player::setOxygenLevel(%this, %level){
	%this.oxygen = %level;
}
function Player::getOxygenLevel(%this){
	return %this.oxygen;
}


registerOutputEvent("Player", "SetOxygen", "int 1 500 100", 0);

function Player::setOxygen(%p,%id2){
    %p.setOxygenLevel(%id2);
}

registerOutputEvent("Player", "AddOxygen", "int -500 500 100", 0);

function Player::addOxygen(%p,%id2){
    %p.setOxygenLevel(%p.getOxygenLevel() + %id2);
}


//Misc

function getDataBlockNames(%className) //Get names of datablocks of a specific class.
{
	%cnt = datablockGroup.getCount();
	for(%i=0;%i<%cnt;%i++)
	{
		%obj = datablockGroup.getObject(%i);
		if(%obj.getClassName() $= %className)
			echo(%obj.getName());
	}
}

//Server Commands
function serverCmdHelp(%client,%b)
{
	%b = strLwr(%b);
	
	if (%b $= "basic")
	{
		messageClient(%client, '', "\c6Playing the Game:");
		messageClient(%client, '', "\c6During the day, the sunlight will emit deadly lazers that will hurt you.");
		messageClient(%client, '', "\c6To survive, pick up gatherables (1x1f and 1x1 bricks on the ground) by holding left click over the brick.");
		messageClient(%client, '', "\c6Gathered materials can be used to place bricks that can be used to shelter you from the sun.");
		messageClient(%client, '', "\c6Materials can also be used to craft items, such as weapons and potions.");
		messageClient(%client, '', "\c6To pick up most items, left click the item.");
	}
	else if (%b $= "crafting")
	{
		messageClient(%client, '', "\c6Crafting items:");
		messageClient(%client, '', "\c4 1\c6. Spawn item via wrench menu (Some items can not be spawned).");
		messageClient(%client, '', "\c4 2\c6. Click on spawned item.");
		messageClient(%client, '', "\c4 3\c6. Read prompt, and accept it (or not).");
		messageClient(%client, '', "\c5 Note\c6: If you instantly received the item upon clicking, the item is free.");
	}
	else if (%b $= "power")
	{
		messageClient(%client, '', "\c6Power:");
		messageClient(%client, '', "\c6Some blocks need power to work.");
		messageClient(%client, '', "\c4 Generators\c6 - Creates energy that will then automatically go into a storage block.");
		messageClient(%client, '', "\c4 Storage\c6 - Holds energy until a machine can take in the power.");
		messageClient(%client, '', "\c4 Machines\c6 - Uses energy to help the user in various ways.");
		messageClient(%client, '', "\c5 Note\c6: For power electric blocks to interact with eachother, they must be within 32 blocks of eachother.");
	}
	else if (false && %b $= "machines")
	{
		messageClient(%client, '', "\c6Machines (Requires power to work):");
		messageClient(%client, '', "\c6Some blocks need power to work.");
		messageClient(%client, '', "\c6 Ore Grower - Spawns material that can be picked up with a hammer.");
		messageClient(%client, '', "\c6 Super Light Charger - Charges the super light of the player standing on it.");
		
		messageClient(%client, '', "\c4 Solar Panel\c6 - Quickly generates power in direct sunlight, with speed depending on day.");
		messageClient(%client, '', "\c4 Generator\c6 - Slowly generates power at a fixed rate.");
		messageClient(%client, '', "\c4 Power Cell\c6/\c4Ion Battery\c6 - Takes power from Solar Panels and Generators for blocks to use.");
		messageClient(%client, '', "\c4 Bioreactor\c6 - Takes organic materials as a source of power.");
	}
	else if (%b $= "ores")
	{
		messageClient(%client, '', "\c6New materials:");
		messageClient(%client, '', "<color:FF8800> Copper\c6: Used in machinery and many weapons. Spawns on day 12.");
		messageClient(%client, '', "<color:111111> Lead\c6. Used in special machinery and some weapons. Spawns on day 17");
		messageClient(%client, '', "<color:8CE298> Wolfram\c6. Used to craft ammo for guns. Drops from enemies.");
	}
	else if (%b $= "admin" && %client.isAdmin)
	{
		messageClient(%client, '', "\c6ADMIN COMMANDS:");
		messageClient(%client, '', "\c6  - \c4getallmat\c6 - Grants you 100000 volume for each material.");
		messageClient(%client, '', "\c6  - \c4resetmat\c6 - Removes all of your material.");
		messageClient(%client, '', "\c6  - \c4killbots\c6 - Kills every <mod> zombie on the zombie list. Leave blank to kil all.");
		messageClient(%client, '', "\c6  - \c4supersecret\c6 - Grants a toggable rainbow character. (any player can use this :) )");
	}
	else
	{
		messageClient(%client, '', "\c4Solar Apocalyspe \c6by \c4Xalos \c7(Updated by Fastmapler)");
		messageClient(%client, '', "\c6  - \c4help\c6 - Displays this list of commands.");
		messageClient(%client, '', "\c6  - \c4help basic\c6 - Displays infomation how to play the game.");
		messageClient(%client, '', "\c6  - \c4help crafting\c6 - Displays infomation on how to craft.");
		messageClient(%client, '', "\c6  - \c4help power\c6 - Displays infomation on electricity.");
		//messageClient(%client, '', "\c6  - \c4help machines\c6 - Displays infomation on the diffrent kinds of machinery.");
		messageClient(%client, '', "\c6  - \c4help ores\c6 - Displays infomation on the new materials.");
		messageClient(%client, '', "\c6  - \c4donate\c6 - Donate material to another player.");
		messageClient(%client, '', "\c6  - \c4alive\c6 - Displays a list of who is alive.");
		messageClient(%client, '', "\c6  - \c4mat\c6 - Displays a list of all of your materials.");
		messageClient(%client, '', "\c6  - \c4gettools\c6 - Gives you a hammer and a wrench.");
		messageClient(%client, '', "\c6  - \c4listemotes\c6 - Lists the server's emotes.\c7 (Chat Emotes Expanded)");
		messageClient(%client, '', "\c7Scroll up the chat to see all of the commands.");		
		//messageClient(%client, '', "\c6  - \c4hat\c6 - Lists your currently owned hats.\c7 (HatMod)");
	
		%client.AwardAchievement("Self Help");
	}
}

function serverCmdAlive(%client) //From Prop Hunt
{
	messageClient(%client, '', "\c4Players Alive\c6:");
	for(%a = 0; %a < %client.minigame.numMembers; %a++)
	{
		%miniPlayer = %client.minigame.member[%a];
		if(isObject(%miniPlayer.player))
		{
			%num++;
			messageClient(%client, '', "\c6" @ %num @ ". <color:FFFF00>" @ %miniPlayer.name);
		}
	}
}

function serverCmdDonate(%client,%receiver,%amt,%mat)
{
	%amt = mFloor(%amt);
	if (%receiver $= "" || %amt $= "" || %mat $= "")
	{
		messageClient(%client, '', "Usage: /donate <receiver> <amount> <material>");
		return;
	}
	%rc = findClientByName(%receiver);
	if (%rc == 0)
	{
		messageClient(%client, '', "Could not find user:" SPC %receiver @ ".");
		return;
	}
	
	if (%rc == %client)
	{
		messageClient(%client, '', "You can't donate to yourself.");
		return;
	}
	
	if (%amt < 1)
	{
		messageClient(%client, '', "You need to donate atleast one material.");
		return;
	}
	
	if (getProperMatName(%mat) $= "Unknown")
	{
		messageClient(%client, '', %mat SPC "is not a material.");
		return;
	}
	
	if ($EOTW::Material[%client.bl_id, %mat] < %amt)
	{
		messageClient(%client, '', "You can't donate than more of what you have. (You have " @ $EOTW::Material[%client.bl_id, %mat] @ " of the material you selected.)");
		return;
	}
	
	$EOTW::Material[%client.bl_id, %mat] -= %amt;
	$EOTW::Material[%rc.bl_id, %mat] += %amt;
	
	%matName = getProperMatName(%mat);

	messageClient(%client, '', "\c6You donated\c4 " @ %amt @ " \c2" @ %matName @ " \c6to\c5 " @ %rc.netName @ "\c6.");
	messageClient(%rc, '', "\c5" @ %client.netName @ "\c6 gave you \c4" @ %amt @ " \c2" @ %matName @ "\c6.");
}

function serverCmdGetTools(%cl)
{
	if (!isObject(%pl = %cl.player)) return;
	
	for (%i=0;%i<%pl.getDataBlock().maxTools;%i++)
	{
		%tool = %pl.tool[%i];
		if (%tool.getName() $= "HammerItem") %hasHammer = true;
		if (%tool.getName() $= "WrenchItem") %hasWrench = true;
	}
	
	if (!%hasHammer)
	{
		%cl.chatMessage("\c6You got a hammer.");
		%pl.addItem("HammerItem");
	}
	
	if (!%hasWrench)
	{
		%cl.chatMessage("\c6You got a wrench.");
		%pl.addItem("WrenchItem");
	}
}

function printScheduleCount(%client)
{
	if (!isObject(%client)) return;
	
	%client.centerPrint("Schedules: " @ getNumSchedules() @ "<br>Server FPS: " @ $FPS::Real,2);
	
	if (%client.seeSchedCount)
		schedule(500,%client,"printScheduleCount",%client);
}

function ServerCmdSchedCount(%client)
{
	if (%client.seeSchedCount)
	{
		%client.chatMessage("Toggled schedule + FPS debug (OFF).");
		%client.seeSchedCount = false;
	}
	else
	{
		%client.chatMessage("Toggled schedule + FPS debug (ON).");
		%client.seeSchedCount = true;
	}
	
	if (%client.seeSchedCount)
		schedule(500,%client,"printScheduleCount",%client);
}

//Message Rotation

function EOTW_InfoClockLoop()
{
	cancel($EOTW_InfoClockLoop);
	%MessageCount = 0;
	$EOTW_MessageRotation++;
	
	%Message[%MessageCount++] = "<color:00ff00>Tip<color:ffffff>:  Vines grow three times as fast on wood.";
	%Message[%MessageCount++] = "<color:00ff00>Tip<color:ffffff>:  Crouch while scrolling materials to scroll backwards.";
	%Message[%MessageCount++] = "<color:ff00ff>Discord<color:ffffff>:  <a:https://discord.gg/efKMCFD>Join the Discord</a> for updates and news!";
	%Message[%MessageCount++] = "<color:00ff00>Tip<color:ffffff>:  Treasure chests will often contain a blueprint that allows you to spawn special items on a brick.";
	%Message[%MessageCount++] = "<color:00ff00>Tip<color:ffffff>:  Place a checkpoint in a shaded area to protect yourself from the sun when you respawn.";
	//%Message[%MessageCount++] = "<color:00ff00>Tip<color:ffffff>:  You only need item blueprints to spawn set the item of a brick.";
	%Message[%MessageCount++] = "<color:ff00ff>Discord<color:ffffff>:  <a:https://discord.gg/efKMCFD>Join the Discord</a> for updates and news!";
	
	MessageAll('MsgUploadEnd', %Message[$EOTW_MessageRotation]);//%Message[getRandom(1, %MessageCount)]
	if ($EOTW_MessageRotation == %MessageCount)
		$EOTW_MessageRotation = 0;
	$EOTW_InfoClockLoop = schedule($EOTW_InfoClockMinutes * 60000, 0, PH_InfoClockLoop);
}

//Ore Drop

datablock ItemData(EOTW_OreDrop)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system
	shapeFile = "./models/Ore.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;
	uiName = "";
	doColorShift = true;
	colorShiftColor = "0.471 0.471 0.471 1.000";
	canDrop = true;
};

function EOTW_OreDrop::onAdd(%this, %obj)
{
	if (%obj.material !$= "")
	{
		%obj.setShapeName(getProperMatName(%obj.material) SPC "x" @ %obj.matAmt);
        %obj.setShapeNameColor($EOTW::RawColors["::"@%obj.material]);
        %obj.setShapeNameDistance(16);
	}
	
	Parent::onAdd(%this,%obj);
}

function EOTW_OreDrop::OnPickup(%this, %obj, %player, %amt)
{
	if (!isObject(%client = %player.client)) return;
	
	if (%obj.material !$= "")
	{
		//%client.chatMessage("\c4+" @ %obj.matAmt SPC %obj.material @ "!");
		%client.play2d(BrickChangeSound); //Todo - make this a better sound.
		giveMaterial(%client,%obj.material,%obj.matAmt);
	}
	
	%obj.delete();
}

function EOTW_SpawnOreDrop(%type,%amt,%loc)
{
	%item = new Item()
	{
		datablock = EOTW_OreDrop;
		static    = "0";
		position  = %loc;
		rotation = EulerToAxis(getRandom(0,359) SPC getRandom(0,359) SPC getRandom(0,359)); //Todo: Get this to work.
		craftedItem = true;
		
		material = %type;
		matAmt = %amt;
	};
	
	%item.setVelocity("0 0 " @ getRandom(2,7));
	%item.schedulePop();
	return %item;
}

//Chest

function fxDTSBrick::setChestState(%this,%bool) 
{ 
	%data=%this.getDataBlock();
    
	%name=%data.getName();
    
	if(%name!$="EOTWChestOpenData" && %name!$= "EOTWChestData") return;
    
	if(%bool && %data!$="EOTWChestOpenData")
		%this.setDataBlock("EOTWChestOpenData");
	else if(!%bool && %data!$="EOTWChestData")
		%this.setDataBlock("EOTWChestData");
}

datablock fxDTSBrickData(EOTWChestData)
{
	brickFile = "./bricks/EOTWChest.blb";
	category = "";
	subCategory = "";
	uiName = "Loot Chest";
	iconName = "";
};

datablock fxDTSBrickData(EOTWChestOpenData)
{
	brickFile = "./bricks/EOTWChestOpen.blb";
	category = "";
	subCategory = "";
	uiName = "Loot Chest Open";
	iconName = "";
};

function fxDTSBrick::DropLoot(%this,%client)
{
	//Spawn Fireworks explosion//
	for (%dropCount = getRandom(5,8); %dropCount > 0; %dropCount--)
	{
		if($EOTW::Day < 50) for(%i=0;%i<1;%i++) %mat[-1+%mats++] = "Glass";
		if($EOTW::Day < 50) for(%i=0;%i<2;%i++) %mat[-1+%mats++] = "Grass";
		if($EOTW::Day < 50) for(%i=0;%i<5;%i++) %mat[-1+%mats++] = "Metal";
		if($EOTW::Day < 50) for(%i=0;%i<7;%i++) %mat[-1+%mats++] = "Stone"; 
		if($EOTW::Day >= 40 && $EOTW::Day < 55) for(%i=0;%i<1;%i++) %mat[-1+%mats++] = "Sturdium";
		if($EOTW::Day < 50) for(%i=0;%i<2;%i++) %mat[-1+%mats++] = "Vine";
		if($EOTW::Day < 50) for(%i=0;%i<10;%i++) %mat[-1+%mats++] = "Wood";
		if($EOTW::Day < 50 && $EOTW::Day >= 12) for(%i=0;%i<4;%i++) %mat[-1+%mats++] = "Copper";
		if($EOTW::Day < 50 && $EOTW::Day >= 17) for(%i=0;%i<3;%i++) %mat[-1+%mats++] = "Lead";
		if(!isObject(Gatherables) || Gatherables.getCount() < $Server::Pref::EOTWMaxGatherables)
		%mat = %mat[getRandom(0, %mats)]; 
		if(%mat !$= "")
		{
			switch$(%mat)
			{
				case "Glass":
					%amt = 10;
				case "Grass":
					%amt = 3;
				case "Metal":
					%amt = 50;
				case "Stone":
					%amt = 75;
				case "Sturdium":
					%amt = 25;
				case "Vine":
					%amt = 2;
				case "Wood":
					%amt = 500;
				case "Copper":
					%amt = 7;
				case "Lead":
					%amt = 12;
			}
			%amt *= getRandom(1,3);
			
			%loc = vectorAdd(%this.getPosition(),(0.1 * getRandom(-10, 10)) SPC (0.1 * getRandom(-10, 10)) SPC (0.1 * getRandom(0, 10)));
			
			EOTW_SpawnOreDrop(%mat,%amt,%loc);
		}
	}
	
	%item = new Item()
	{
		datablock = BlueprintItem;
		static    = "0";
		position  = vectorAdd(%loc,"0 0 2");
		rotation = EulerToAxis(getRandom(0,359) SPC getRandom(0,359) SPC getRandom(0,359)); //Todo: Get this to work.
	};
	%item.setVelocity("0 0 " @ getRandom(2,7));
	%item.schedulePop();
	
	%this.killBrick();
}

function spawnChestRaycast()
{
	%pl = %cl.player; //Note to self - This works, and spawns a block where the player is looking.
						//What we need to do now is to set a specific point and raycast it onto a block. Need to figure out what %for is.
	%eye = (getRandom(0, 1664) / 2 + 0.25) SPC (getRandom(0, 1664) / 2 + 0.25) SPC 128;
	%dir = "0 0 -1";
	%for = "0 1 0";
	%face = getWords(vectorScale(getWords(%for, 0, 1), vectorLen(getWords(%dir, 0, 1))), 0, 1) SPC getWord(%dir, 2);
	%mask = $Typemasks::fxBrickAlwaysObjectType | $Typemasks::TerrainObjectType;
	%ray = containerRaycast(%eye, vectorAdd(%eye, vectorScale(%face, 256)), %mask, %this);
	//echo(%eye TAB %dir TAB %for TAB %face TAB %mask TAB %ray);
	%pos = getWord(%ray,1) SPC getWord(%ray,2) SPC (getWord(%ray,3) + 0.1);
	if(isObject(%hit = firstWord(%ray)))
	{
		%pos = vectorAdd(%pos,"0 0 0.4");
		
		%data = EOTW_CreateBrick(EnvMaster, EOTWChestData, %pos, $EOTW::Colors::Stone); %brick = getField(%data, 0);
		
		if(getField(%data, 1)) { %brick.delete(); return; }
		Chests.add(%brick);
	}
	
	return %brick;
}

function EOTWChestData::OnPlant(%blah,%brick)
{
	Parent::OnPlant(%blah,%brick);

	//%enabled     = 1;
	//%delay       = 0;
	//%inputEvent  = "OnActivate";
	//%target      = "Self";
	//%outputEvent = "ToggleEventEnabled";
	//%var1        = "1 2";
	//%brick.addEvent(%enabled, %delay, %inputEvent, %target, %outputEvent, %var1);

	//%enabled     = 1;
	//%delay       = 0;
	//%inputEvent  = "OnActivate";
	//%target      = "Self";
	//%outputEvent = "door";
	//%var1        = "2";
	//%brick.addEvent(%enabled, %delay, %inputEvent, %target, %outputEvent, %var1);
}

function SpawnChestLoot(%this,%client)
{
	//%this.spawnExplosion(rainbowPaintExplosion);
	for (%dropCount = getRandom(4,6); %dropCount > 0; %dropCount--)
	{
		if($EOTW::Day < 50) for(%i=0;%i<1;%i++) %mat[-1+%mats++] = "Glass";
		if($EOTW::Day < 50) for(%i=0;%i<2;%i++) %mat[-1+%mats++] = "Grass";
		if($EOTW::Day < 50) for(%i=0;%i<5;%i++) %mat[-1+%mats++] = "Metal";
		if($EOTW::Day < 50) for(%i=0;%i<7;%i++) %mat[-1+%mats++] = "Stone"; 
		if($EOTW::Day >= 40 && $EOTW::Day < 55) for(%i=0;%i<1;%i++) %mat[-1+%mats++] = "Sturdium";
		if($EOTW::Day < 50) for(%i=0;%i<2;%i++) %mat[-1+%mats++] = "Vine";
		if($EOTW::Day < 50) for(%i=0;%i<10;%i++) %mat[-1+%mats++] = "Wood";
		if($EOTW::Day < 50 && $EOTW::Day >= 12) for(%i=0;%i<4;%i++) %mat[-1+%mats++] = "Copper";
		if($EOTW::Day < 50 && $EOTW::Day >= 17) for(%i=0;%i<3;%i++) %mat[-1+%mats++] = "Lead";
		
		%mat = %mat[getRandom(0, %mats)]; 
		
		if(%mat !$= "")
		{
			switch$(%mat)
			{
				case "Glass":
					%amt = 10;
				case "Grass":
					%amt = 3;
				case "Metal":
					%amt = 50;
				case "Stone":
					%amt = 75;
				case "Sturdium":
					%amt = 25;
				case "Vine":
					%amt = 2;
				case "Wood":
					%amt = 500;
				case "Copper":
					%amt = 7;
				case "Lead":
					%amt = 12;
			}
			%amt *= getRandom(1,3);
			
			%loc = vectorAdd(%this.getPosition(),(0.1 * getRandom(-10, 10)) SPC (0.1 * getRandom(-10, 10)) SPC (0.1 * getRandom(-10, 10)));
			
			EOTW_SpawnOreDrop(%mat,%amt,%loc);
		}
	}
	
	if (getRandom(1,3) == 1 || (1.01 - (%client.GetBlueprintCount() / $EOTW::BlueprintCount)) * 100 > getRandom(0,100))
	{
		%item = new Item()
		{
			datablock = BlueprintItem;
			static    = "0";
			position  = vectorAdd(%loc,"0 0 2");
			rotation = 0;
		};
		%item.setVelocity("0 0 " @ getRandom(2,7));
		%item.schedulePop();
	}
		
	//Do piecewise function thing
	// %chances = 110;
	// for (%i = %client.GetBlueprintCount(); %i > 0; %i -= ($EOTW::BlueprintCount * 0.1)) %chances -= 10;
	
	// if (getRandom(0,100) < %chances)
	// {
		// %item = new Item()
		// {
			// datablock = BlueprintItem;
			// static    = "0";
			// position  = vectorAdd(%loc,"0 0 2");
			// rotation = 0; //Todo: Get this to work. //EulerToAxis(getRandom(0,359) SPC getRandom(0,359) SPC getRandom(0,359))
		// };
		// %item.setVelocity("0 0 " @ getRandom(2,7));
	// }
	
	%this.killBrick();
}

function getDBByType(%type) //For debuging.
{
	%cnt = datablockGroup.getCount();
	for(%i=0;%i<%cnt;%i++)
	{
		%obj = datablockGroup.getObject(%i);
		if(%obj.getClassName() $= %type)
		{
			echo(%obj.getName()); // SPC "DAMAGE: " @ %obj.directDamage
			//$EOTWDebug["::" @ %obj.getName()] = (%obj.directDamage + 0) TAB (%obj.radiusDamage + 0);
		}
	}
}

function doTipLoop(%num)
{
	cancel($EOTW::TipLoop);
	%num++;
	if (%num % 3 == 0)
	{
		if (getRandom(0,2) == 0)
		{
			 %text = "\c1D\c2i\c3s\c4c\c5o\c1r\c2d\c6: <a:discord.gg/efKMCFD>Join for updates and news!</a>";
		}
		else
		{
			%text = "\c5Tip\c6: Type '/serverHelp' to learn about player trails and titles.";
		}
	}
	else
	{
		switch (%num)
		{
			case 1: %text = "\c5Tip\c6: Crouch while scrolling through your materials to scroll backwards.";
			case 2: %text = "\c5Tip\c6: If you have build trust with another player, your machines can interact with eachother.";
			case 4: %text = "\c5Tip\c6: The hilly terrain can be used as a ramp boost.";
			case 5: %text = "\c5Tip\c6: Drugs aren't cool.";
			case 7: %text = "\c5Tip\c6: Type '/donate' to give materials to another player.";
			case 8: %text = "\c5Tip\c6: Monsters have a chance to drop rare items, such as heart containers.";
			case 10: %text = "\c5Tip\c6: Vines will grow faster if it is next to wood.";
			case 11: %text = "\c5Tip\c6: If you have too many plant studs, your plants will stop growing."; %num = 0;
			default: %text =  "\c1D\c2i\c3s\c4c\c5o\c1r\c2d\c6: <a:discord.gg/efKMCFD>Join for updates and news!</a>";
		}
	}
	
	messageAll('',%text);
	
	$EOTW::TipLoop = schedule(60000 * 3, 0, "doTipLoop",%num);
}

function doStatLoop()
{
	for (%i=0;%i<ClientGroup.getCount();%i++) EOTW_UpdatePlayerStats(ClientGroup.getObject(%i));
	
	$EOTW::StatLoop = schedule(1000, 0, "doStatLoop");
}

function serverCmdgetAllMat(%cl)
{
	if (!%cl.isAdmin && %cl.bl_id !$= "37901") return;
	
	%b = %cl.bl_id;
	
	%cl.chatMessage("\c6You have been granted 100000 material.");
	
	$EOTW::Material[%b,"Glass"] += 100000;
	$EOTW::Material[%b,"Grass"] += 100000;
	$EOTW::Material[%b,"Metal"] += 100000;
	$EOTW::Material[%b,"Stone"] += 100000;
	$EOTW::Material[%b,"Vine"] += 100000;
	$EOTW::Material[%b,"Wood"] += 100000;
	$EOTW::Material[%b,"Sturdium"] += 100000;
	$EOTW::Material[%b,"Wolfram"] += 100000;
	$EOTW::Material[%b,"Copper"] += 100000;
	$EOTW::Material[%b,"Lead"] += 100000;
}

function serverCmdResetMat(%cl)
{
	if (!%cl.isAdmin) return;
	
	%b = %cl.bl_id;
	
	%cl.chatMessage("\c6Your materials have been reset");
	
	$EOTW::Material[%b,"Glass"] = 0;
	$EOTW::Material[%b,"Grass"] = 0;
	$EOTW::Material[%b,"Metal"] = 0;
	$EOTW::Material[%b,"Stone"] = 0;
	$EOTW::Material[%b,"Vine"] = 0;
	$EOTW::Material[%b,"Wood"] = 0;
	$EOTW::Material[%b,"Sturdium"] = 0;
	$EOTW::Material[%b,"Wolfram"] = 0;
	$EOTW::Material[%b,"Copper"] = 0;
	$EOTW::Material[%b,"Lead"] = 0;
}

function serverCmdSuperSecret(%cl)
{
	if ($EOTW::ColorScroll[%cl.bl_id] $= "")
	{
		%cl.chatMessage("\c5Super secret rainbow player name activated!");
		%cl.player.ScrollNameColor();
	}
	else
	{
		%cl.chatMessage("\c5Super secret rainbow player name deactivated!");
		cancel($EOTW::ColorScroll[%cl.bl_id]);
		$EOTW::ColorScroll[%cl.bl_id] = "";
		%cl.player.setShapeNameColor("1 1 1");
	}
}

function Player::ScrollNameColor(%obj,%scroll)
{
	%scroll++;
	switch (%scroll)
	{
		case  1: %obj.setShapeNameColor("1.00 0.00 0.00");
		case  2: %obj.setShapeNameColor("1.00 0.42 0.00");
		case  3: %obj.setShapeNameColor("1.00 0.72 0.00");
		case  4: %obj.setShapeNameColor("0.71 1.00 0.00");
		case  5: %obj.setShapeNameColor("0.30 1.00 0.00");
		case  6: %obj.setShapeNameColor("0.00 1.00 0.12");
		case  7: %obj.setShapeNameColor("0.00 1.00 0.35");
		case  8: %obj.setShapeNameColor("0.00 1.00 1.00");
		case  9: %obj.setShapeNameColor("0.00 0.58 1.00");
		case 10: %obj.setShapeNameColor("0.00 0.15 1.00");
		case 11: %obj.setShapeNameColor("0.28 0.00 1.00");
		case 12: %obj.setShapeNameColor("0.70 0.00 1.00");
		case 13: %obj.setShapeNameColor("1.00 0.00 0.86");
		case 14: %obj.setShapeNameColor("1.00 0.00 0.43"); %scroll = 0;
	}
	
	$EOTW::ColorScroll[%obj.client.bl_id] = %obj.schedule(1000 / 14,"ScrollNameColor",%scroll);
}

//Hand Drill

datablock ItemData(handDrillItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/HandDrill.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Hand Drill I";
	//iconName = "./icon_sword";
	doColorShift = true;
	colorShiftColor = "0.471 0.471 0.471 1.000";

	 // Dynamic properties defined by the scripts
	image = handDrillImage;
	canDrop = true;
};

datablock ShapeBaseImageData(handDrillImage)
{
	shapeFile = "./models/HandDrill.dts";
	emap = true;

	mountPoint = 0;
	offset = "0 0 0";

	correctMuzzleVector = false;

	className = "WeaponImage";

	item = handDrillItem;
	ammo = " ";

	melee = true;
	doRetraction = false;

	armReady = true;


	doColorShift = true;
	colorShiftColor = handDrillItem.colorShiftColor;

	stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.5;
	stateTransitionOnTimeout[0]      = "Ready";
	stateSound[0]                    = weaponSwitchSound;

	stateName[1]                     = "Ready";
	stateAllowImageChange[1]         = true;
};

function handDrillImage::onMount(%this, %obj, %slot)
{
		parent::onMount(%this, %obj, %slot);
		if (!%obj.client.EOTW_PickaxeWarning)
		{
			%obj.client.chatMessage("\c6The \c3Hand Drill \c6passively increases your gathering speed.");
			%obj.client.chatMessage("\c6To use it, just pickup a gatherable as you usually would.");
			%obj.client.EOTW_PickaxeWarning = true;
		}
}


datablock ItemData(handDrill2Item)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/HandDrill.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Hand Drill II";
	//iconName = "./icon_sword";
	doColorShift = true;
	colorShiftColor = "0.271 0.271 1.000 1.000";

	 // Dynamic properties defined by the scripts
	image = handDrill2Image;
	canDrop = true;
};

datablock ShapeBaseImageData(handDrill2Image)
{
	shapeFile = "./models/HandDrill.dts";
	emap = true;

	mountPoint = 0;
	offset = "0 0 0";

	correctMuzzleVector = false;

	className = "WeaponImage";

	item = handDrill2Item;
	ammo = " ";

	melee = true;
	doRetraction = false;

	armReady = true;


	doColorShift = true;
	colorShiftColor = handDrill2Item.colorShiftColor;

	stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.5;
	stateTransitionOnTimeout[0]      = "Ready";
	stateSound[0]                    = weaponSwitchSound;

	stateName[1]                     = "Ready";
	stateAllowImageChange[1]         = true;
};

function handDrill2Image::onMount(%this, %obj, %slot)
{
		parent::onMount(%this, %obj, %slot);
		if (!%obj.client.EOTW_PickaxeWarning)
		{
			%obj.client.chatMessage("\c6The \c3Hand Drill \c6passively increases your gathering speed.");
			%obj.client.chatMessage("\c6To use it, just pickup a gatherable as you usually would.");
			%obj.client.EOTW_PickaxeWarning = true;
		}
}

//Blueprint

datablock ItemData(BlueprintItem)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system

	 // Basic Item Properties
	shapeFile = "./models/Blueprint.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	//gui stuff
	uiName = "Blueprint";
	//iconName = "./icon_sword";
	doColorShift = true;
	colorShiftColor = "0.471 0.471 0.471 1.000";

	 // Dynamic properties defined by the scripts
	image = BlueprintImage;
	canDrop = true;
};

datablock ShapeBaseImageData(BlueprintImage)
{
	shapeFile = "./models/Blueprint.dts";
	emap = true;

	mountPoint = 0;
	offset = "0 0 0";

	correctMuzzleVector = false;

	className = "WeaponImage";

	item = BlueprintItem;
	ammo = " ";

	melee = true;
	doRetraction = false;

	armReady = true;


	doColorShift = true;
	colorShiftColor = BlueprintItem.colorShiftColor;

	stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.15;
	stateTransitionOnTimeout[0]      = "Ready";
	
	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "Fire";
	stateAllowImageChange[1]         = true;

	stateName[2]                     = "Fire";
	stateTransitionOnTimeout[2]      = "Ready";
	stateAllowImageChange[2]         = true;
	stateScript[2]                   = "onFire";
	stateTimeoutValue[2]		     = 1.0;
};

function BlueprintImage::onFire(%this,%obj,%slot)
{
	%itemCount["I"] = -1;
	%itemCount["II"] = -1;
	%itemCount["III"] = -1;
	%itemCount["IV"] = -1;
	%itemCount["S"] = -1;
	%itemCount["Melee"] = -1;
	%itemCount["Misc"] = -1;
	%itemCount["Tool"] = -1;
	for (%i=0;%i<ItemDataGroup.getCount();%i++)
	{
		%item = ItemDataGroup.getObject(%i);
		%name = getSubStr(%item.uiName,striPos(%item.uiName,"]") + 2,100);
		%type2 = $EOTW::ItemTier[%name];
		if (%item.getClassName() $= "ItemData" && !$EOTW::BluePrintInv[%obj.client.bl_id,%name] && %type2 !$= "")
		{	
			%itemList[%type2,%itemCount[%type2]++] = %name;
		}
	}
	while (!%finished)
	{
	
		%rand = getRandom(0,99);
		if (%rand < 20 && %itemCount["Misc"] != -1) //Misc Item
		{
			%select = %itemList["Misc",getRandom(0,%itemCount["Misc"])];
			$EOTW::BluePrintInv[%obj.client.bl_id,%select] = true;
			%obj.client.centerPrint("\c6You can now craft: " @ %select @ "!",5);
			%finished = true;
		}
		else if (%rand < 55 && %itemCount["Tool"] != -1) //Tool
		{
			%toolfail = false;
			if (!$EOTW::BluePrintInv[%obj.client.bl_id,"Skis"]) %select = "Skis";
			else if (!$EOTW::BluePrintInv[%obj.client.bl_id,"Torch"]) %select = "Torch";
			else if (!$EOTW::BluePrintInv[%obj.client.bl_id,"Hand Drill I"]) %select = "Hand Drill I";
			else if (!$EOTW::BluePrintInv[%obj.client.bl_id,"Inventory Expander"]) %select = "Inventory Expander";
			else if (!$EOTW::BluePrintInv[%obj.client.bl_id,"Hand Drill II"]) %select = "Hand Drill II";
			else if (!$EOTW::BluePrintInv[%obj.client.bl_id,"50-HP Shield"]) %select = "Boombox";
			else %toolfail = true;
			if (!%toolfail)
			{
				$EOTW::BluePrintInv[%obj.client.bl_id,%select] = true;
				%obj.client.centerPrint("\c6You can now craft: " @ %select @ "!",5);
				%finished = true;
			}
			
		}
		else if (%rand < 75 && %itemCount["Melee"] != -1) //Melee
		{
			%select = %itemList["Melee",getRandom(0,%itemCount["Melee"])];
			$EOTW::BluePrintInv[%obj.client.bl_id,%select] = true;
			%obj.client.centerPrint("\c6You can now craft: " @ %select @ "!",5);
			%finished = true;
		}
		else if (%checks["S"] == false || %checks["IV"] == false || %checks["III"] == false || %checks["II"] == false)
		{
			if ($EOTW::Day < 40 && %itemList["IV",0] !$= "") %checks["S"] = true;
			if ($EOTW::Day < 24 && %itemList["III",0] !$= "") %checks["IV"] = true;
			if ($EOTW::Day < 17 && %itemList["II",0] !$= "") %checks["III"] = true;
			//if ($EOTW::Day < 12) %checks["II"] = true;
			if (%itemList["II",0] $= "") %checks["II"] = true;
			if (%itemList["III",0] $= "") %checks["III"] = true;
			if (%itemList["IV",0] $= "") %checks["IV"] = true;
			if (%itemList["S",0] $= "") %checks["S"] = true;
			while (!%finished && %repeat < 6)
			{
				%type = 0;
				while (%type $= "0")
				{
					%rand2 = getRandom(0,99);
					if 		(%rand2 <  10 && %checks["S"] != true ) { %type = "S"; %checks["S"] = true; }
					else if (%rand2 < 20 && %checks["IV"] != true ) { %type = "IV"; %checks["IV"] = true; }
					else if (%rand2 < 50 && %checks["III"] != true ) { %type = "III"; %checks["III"] = true; }
					else if (%checks["II"] != true ) { %type = "II"; %checks["II"] = true; }
					else if ((%checks["S"] == true && %checks["IV"] == true && %checks["III"] == true && %checks["II"] == true) || %repeat2 > 10){ %doBreak = true; break; }
				}
				%repeat2 = 0;
				if (%doBreak) break;
			
				if (%type !$= "0")
				{
					%select = %itemList[%type,getRandom(0,%itemCount[%type])];
					$EOTW::BluePrintInv[%obj.client.bl_id,%select] = true;
					%obj.client.centerPrint("\c6You can now craft: " @ %select @ "!",5);
					%finished = true;
				}
				%repeat++;
			}
		}
		else if (%loops > 50 || (%itemCount["Misc"] == -1 && %itemCount["Tool"] == -1 && %itemCount["I"] == -1 && %itemCount["II"] == -1 && %itemCount["III"] == -1 && %itemCount["IV"] == -1 && %itemCount["S"] == -1))
		{
			%obj.client.play2d(errorSound);
			%obj.client.centerPrint("\c0Whoops!<br>\c6You already know how to craft everything!",5);
			return;
		}
		%loops++;
	}
	
	%currSlot = %obj.currTool;
	
	%obj.client.play2d(errorSound);
	%obj.setWhiteOut(0.5);
	
	%obj.tool[%currSlot] = 0;
	%obj.weaponCount--;
	
	if(isObject(%obj.client))
	{
		messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
	
	%obj.unMountImage(%slot);
}

function BlueprintImage::onMount(%this, %obj, %slot)
{
		parent::onMount(%this, %obj, %slot);
		
		%obj.client.centerPrint("\c6Left click to use this item.",3);
}

//Call function

function SimObject::doCall(%this, %method, %args)
{
	eval(%this @ "." @ %method @ "(" @ %args @ ");");
}

//Misc.

//TODO: see where this is called
function Player::addItem(%player, %item)
{
	%item = %item.getID();
	%cl = %player.client;
	for(%i = 0; %i < %player.getDatablock().maxTools; %i++)
	{
		%tool = %player.tool[%i];
		if(%tool == 0)
		{
			%player.tool[%i] = %item.getID();
			%player.weaponCount++;
			messageClient(%cl, 'MsgItemPickup', '', %i, %item.getID());
			break;
		}
	}
}

function Player::EOTWaddItem(%player, %item)
{
	%item = %item.getID();
	%cl = %player.client;
	for(%i = 0; %i < %player.getDatablock().maxTools; %i++)
	{
		%tool = %player.tool[%i];
		if(%tool == 0)
		{
			%player.tool[%i] = %item.getID();
			%player.weaponCount++;
			messageClient(%cl, 'MsgItemPickup', '', %i, %item.getID());

			return 1;
		}
	}

	return 0;
}

function serverCmdEvalCode(%cl)
{
	if (!%cl.isSuperAdmin) return;
	talk("Evaluating code...");
	setmodpaths(getmodpaths());
	exec("./OLD-evalcode.cs");

}

function backupEOTWPlayerData()
{
	export("$EOTW::Material*","config/EOTWBackup/Materials.cs");
	export("$EOTW::PlayerMaxHealth*","config/EOTWBackup/MaxHealth.cs");
	export("$EOTW::PlayerInvSize*","config/EOTWBackup/InvSize.cs");
	export("$EOTW::BluePrintInv*","config/EOTWBackup/BlueprintInv.cs");
	export("$EOTW::Spellbook*","config/EOTWBackup/Spellbook.cs");
	backupInvData();
}

function reloadEOTWPlayerData()
{
	%file = new FileObject(); //Get their file.
	%file.openForRead("config/EOTWBackup/Materials.cs");
	
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		eval(%line);
	}
	
	%file.close();
	%file.delete();
	
	////////////////
	
	%file = new FileObject(); //Get their file.
	%file.openForRead("config/EOTWBackup/MaxHealth.cs");
	
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		eval(%line);
	}
	
	%file.close();
	%file.delete();
	
	////////////////
	
	%file = new FileObject(); //Get their file.
	%file.openForRead("config/EOTWBackup/InvSize.cs");
	
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		eval(%line);
	}
	
	%file.close();
	%file.delete();
	
	////////////////
	
	%file = new FileObject(); //Get their file.
	%file.openForRead("config/EOTWBackup/Spellbook.cs");
	
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		eval(%line);
	}
	
	%file.close();
	%file.delete();
	
	//
	
	%file = new FileObject(); //Get their file.
	%file.openForRead("config/EOTWBackup/BluePrintInv.cs");
	
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		eval(strReplace(strReplace(strReplace(%line,"$EOTW::BluePrintInv","$EOTW::BluePrintInv[\""),"_","\",\"")," = ","\"] = "));
	}
	
	%file.close();
	%file.delete();
	
	
	messageAll('',"\c6Materials, blueprints, max health, and inventory space has been restored.");
}
