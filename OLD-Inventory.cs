//new SimSet(InventoryHandler);

function ServerCmdInv(%client) { ServerCmdInventory(%client); }

function ServerCmdInventory(%client)
{
	%inv = "InventoryHandler_" @ %client.bl_id;
	if (!isObject(%inv))
		%inv = new SimSet("InventoryHandler_" @ %client.bl_id);
	
	%client.chatMessage("\c6Inventory:");
	for (%i = 0; %i < %inv.getCount(); %i++)
	{
		%item = %inv.getObject(%i);
		%client.chatMessage("\c3" @ %item.getName() @ " \c6x\c3" @ (%item.count + 0));
	}
}

function SimSet::getObjectByName(%obj,%name)
{
	for (%i = 0; %i < %obj.getCount(); %i++)
	{
		if (%obj.getObject(%i).getName() $= %name)
			return %obj.getObject(%i).getID();
	}
	return -1;
}

function SimSet::EOTWInv_ModItem(%inv,%name,%count)
{
	%item = %inv.getObjectByName(%name);
	
	if (!isObject(%item))
	{
		if (%count <= 0) return "oof";
		%inv.add(new SimObject(%name));
		%item = %inv.getObjectByName(%name);
	}
	%item.count += %count;
	
	if (%item.count <= 0)
	{
		%tempCount = %item.count;
		%item.delete();
		return %tempCount;
	}
	
	%sort = new GuiTextListCtrl();

	for(%i = 0; %i < %inv.getCount(); %i++)
		%sort.addRow(0, %inv.getObject(%i).getName());
		
	%sort.sort(0, 1);
	
	for (%i = 0; %i < %sort.rowCount(); %i++)
		%inv.pushToBack(%inv.getObjectByName(%sort.getRowText(%i)));
		
	%sort.delete();
	
	return %item.count;
}

function SimSet::EOTWInv_GetItem(%inv,%name)
{
	%item = %inv.getObjectByName(%name);
	
	if (isObject(%item))
		return %item.count;
	else
		return 0;
}

function backupInvData() //Throw this into the actual backup function.
{
	for (%i = 0; %i < ClientGroup.getCount(); %i++)
	{
		%client = ClientGroup.getObject(%i);
		%inv = ("InventoryHandler_" @ %client.bl_id).getID();
		if (!isObject(%inv)) continue;
		
		%file = new FileObject();
		%file.openForWrite("config/EOTWBackup/Inventory/" @ %client.bl_id @ ".txt");
		for (%j = 0; %j < %inv.getCount(); %j++)
		{
			%item = %inv.getObject(%j);
			%file.writeLine(%item.getName() TAB %item.count);
		}
		
		%file.close();
		%file.delete();
	}
}

function loadInvData(%client)
{
	%file = new FileObject();
	%file.openForRead("config/EOTWBackup/Inventory/" @ %client.bl_id @ ".txt");
	
	%inv = "InventoryHandler_" @ %client.bl_id;
	if (isObject(%inv))
	{
		%inv.deleteAll();
		%inv.delete();
	}
	
	%inv = new SimSet("InventoryHandler_" @ %client.bl_id);
		
	while (!%file.isEOF())
	{
		%line = %file.readLine();
		%inv.add(new SimObject(getField(%line,0)) { count = getField(%line,1); });
	}
	
	%file.close();
	%file.delete();
}

$EOTW::ItemRecipe["Ashes"] = "Mat_Wood" TAB "200";
$EOTW::ItemRecipe["Coal"] = "Mat_Stone" TAB "50";
$EOTW::ItemRecipe["Blank Rune"] = "Mat_Stone" TAB "20" TAB "Essence" TAB "1";
$EOTW::ItemRecipe["Copper Ingot"] = "Mat_Copper" TAB "45" TAB "Coal" TAB "1";
$EOTW::ItemRecipe["Metal Ingot"] = "Mat_Metal" TAB "300" TAB "Coal" TAB "2";
$EOTW::ItemRecipe["Lead Ingot"] = "Mat_Lead" TAB "150" TAB "Coal" TAB "3";
$EOTW::ItemRecipe["Wolfram Ingot"] = "Mat_Wolfram" TAB "333" TAB "Coal" TAB "2";
$EOTW::ItemRecipe["Microchip"] = "Copper Ingot" TAB "1";
$EOTW::ItemRecipe["Plant Bundle"] = "Mat_Vine" TAB "10" TAB "Mat_Grass" TAB "5";
$EOTW::ItemRecipe["Vial"] = "Mat_Glass" TAB "5";

function GameConnection::HasRequiredItems(%client,%item)
{
	%blid = %client.bl_id;
	for (%i = 0; getField($EOTW::ItemRecipe[%item], %i) !$= ""; %i += 2)
	{
		%part = getField(%item,%i);
		%amt  = getField(%item,%i + 1);
		if (strPos(%part,"Mat_") > -1)
		{
			%part = getSubStr(%part,3,strLen(%part));
			if ($EOTW::Material[%blid,%part] $= "")
				$EOTW::Material[%blid,%part] = 0; //So we atleast have a number.
		
			%item = $EOTW::ItemRecipe[%item];
			if ($EOTW::Material[%blid,%part] < %amt)
				return false;
		}
		else
		{
			if (("InventoryHandler_" @ %client.bl_id).EOTWInv_GetItem(%part) < %amt)
				return false;
		}
		
		
		if (%i > 100)
		{
			warn("uh oh");
			break;
		}
	}
	
	return true;
}

datablock ItemData(EOTW_ItemDrop)
{
	category = "Weapon";  // Mission editor category
	className = "Weapon"; // For inventory system
	shapeFile = "./models/Cube.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;
	uiName = "";
	doColorShift = true;
	colorShiftColor = "0.700 0.700 0.700 1.000";
	canDrop = true;
};

function EOTW_ItemDrop::onAdd(%this, %obj)
{
	if (%obj.item !$= "")
	{
		%obj.setShapeName(%obj.item SPC "x" @ %obj.itemAmt);
        %obj.setShapeNameColor("1 0 0");
        %obj.setShapeNameDistance(16);
		%obj.rotate = true;
	}
	
	Parent::onAdd(%this,%obj);
}

function EOTW_ItemDrop::OnPickup(%this, %obj, %player, %amt)
{
	if (!isObject(%client = %player.client)) return;
	
	if (%obj.item !$= "")
	{
		%client.chatMessage("\c4+" @ %obj.itemAmt SPC %obj.item);
		%client.play2d(BrickChangeSound); //Todo - make this a better sound.
		("InventoryHandler_" @ %client.bl_id).EOTWInv_ModItem(%obj.item,%obj.itemAmt);
	}
	
	%obj.delete();
}
